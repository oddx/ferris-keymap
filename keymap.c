#include QMK_KEYBOARD_H

enum ferris_layers {
  QWERTY,
  NUM
};

enum ferris_tap_dances {
  TD_QESC,
  TD_VGRV,
  TD_SPTA,
  TD_PQOU
};

#define KC_CTSL RCTL_T(KC_SLSH)
#define KC_CTLZ LCTL_T(KC_Z)
#define KC_ALTX LALT_T(KC_X)
#define KC_ALTD RALT_T(KC_DOT)
#define KC_LSHA LSFT_T(KC_A)
#define KC_SHSC RSFT_T(KC_SCLN)
#define KC_GUIC GUI_T(KC_C)
#define KC_GCOM GUI_T(KC_COMM)
#define KC_GPLY GUI_T(KC_MPLY)
#define KC_ARWD LALT_T(KC_MRWD)

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [QWERTY] = LAYOUT(
    TD_QESC, KC_W,    KC_E,    KC_R,    KC_T,            KC_Y,    KC_U,  KC_I,    KC_O,    KC_P,
    KC_LSHA, KC_S,    KC_D,    KC_F,    KC_G,            KC_H,    KC_J,  KC_K,    KC_L,    KC_SHSC,
    KC_CTLZ, KC_ALTX, KC_GUIC, TD_VGRV, KC_B,            KC_N,    KC_M,  KC_GCOM, KC_ALTD, KC_CTSL,
                                    MO(1),   KC_BSPC, TD_SPTA,    KC_ENT
  ),
  
  [NUM] = LAYOUT(
    KC_TRNS, KC_VOLD, KC_UP,   KC_VOLU,    KC_T,         KC_LBRC, KC_1,  KC_2,    KC_3,   KC_MINS,
    KC_TRNS, KC_LEFT, KC_DOWN, KC_RGHT,    KC_G,         KC_BSLS, KC_4,  KC_5,    KC_6,   KC_0,
    KC_TRNS, KC_MRWD, KC_MPLY, KC_MFFD,    KC_B,         KC_RBRC, KC_7,  KC_8,    KC_9,   KC_EQL,
                                    KC_TRNS, KC_TRNS, KC_RSFT,    KC_PSCR
  ),
};

qk_tap_dance_action_t tap_dance_actions[] = {
    // Tap once for X, twice for Y
    [TD_QESC] = ACTION_TAP_DANCE_DOUBLE(KC_Q, KC_ESC),
    [TD_VGRV] = ACTION_TAP_DANCE_DOUBLE(KC_V, KC_GRV),
    [TD_PQOU] = ACTION_TAP_DANCE_DOUBLE(KC_P, KC_QUOT),
    [TD_SPTA] = ACTION_TAP_DANCE_DOUBLE(KC_SPC, KC_TAB)
};

void keyboard_post_init_user(void) {
   setPinOutput(F0);
   writePin(F0, 0);
}
